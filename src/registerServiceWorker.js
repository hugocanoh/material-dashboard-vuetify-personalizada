import { register } from 'register-service-worker';
import Swal from 'sweetalert2';

// informamos al usuario si hay un nuevo SW
const notifyUserAboutUpdate = (worker) => {
  Swal.fire('Nueva version', 'Se ha publicado una actualización del sistema!', 'success')
    .then(() => {
      worker.postMessage({ action: 'skipWaiting' });
    })
    .catch(() => {});
};

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      // console.log('App is being served from cache by a service worker.');
    },
    registered () {
      // console.log('Service worker has been registered.');
    },
    cached () {
      // console.log('Content has been cached for offline use.');
    },
    updatefound () {
      // console.log('New content is downloading.');
    },
    updated (regis) {
      // console.log('New content is available; please refresh.');
      notifyUserAboutUpdate(regis.waiting);
    },
    offline () {
      // console.log('No internet connection found. App is running in offline mode.');
    },
    error () {
      // console.error('Error during service worker registration:', error);
    },
  });
  // actualizamos la pagina si es necesario cargar una nueva versión.
  let refreshing;
  navigator.serviceWorker.addEventListener('controllerchange', () => {
    if (refreshing) return;
    window.location.reload();
    refreshing = true;
  });
}
