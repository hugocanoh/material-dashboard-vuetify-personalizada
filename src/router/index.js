import Vue from 'vue';
import Router from 'vue-router';
import store from '@/store';

// Routes
import paths from './paths';

function route (path) {
  return {
    path: path.path,
    name: path.name || path.view,
    meta: path.meta || {},
    component: (resolve) => import(`@/views${path.folder}/${path.view}.vue`).then(resolve),
  };
}

Vue.use(Router);

// Create a new router
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      meta: { requiresAuth: false },
      children: paths.map((path) => route(path)).concat([
        { path: '*', redirect: '/' },
      ]),
    },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { selector: to.hash };
    }
    return { x: 0, y: 0 };
  },
});

// Controlar si las rutas necesitan autenticación
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (typeof store.getters.tokenUser === 'object' && store.getters.tokenUser.access_token) {
      next();
      return;
    }
    next('/login');
  } else {
    next();
  }
});

export default router;
