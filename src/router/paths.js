/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  // Dashboard
  {
    name: 'Dashboard',
    path: '',
    view: 'Dashboard',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  // Pages
  {
    name: 'User Profile',
    path: 'pages/user',
    view: 'UserProfile',
    folder: '/dashboard',
    meta: { requiresAuth: true },
  },
  {
    name: 'Notifications',
    path: 'components/notifications',
    view: 'Notifications',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  {
    name: 'Icons',
    path: 'components/icons',
    view: 'Icons',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  {
    name: 'Typography',
    path: 'components/typography',
    view: 'Typography',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  // Tables
  {
    name: 'Regular Tables',
    path: 'tables/regular-tables',
    view: 'RegularTables',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  // Maps
  {
    name: 'Google Maps',
    path: 'maps/google-maps',
    view: 'GoogleMaps',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
  // Upgrade
  {
    name: 'Upgrade',
    path: 'upgrade',
    view: 'Upgrade',
    folder: '/dashboard',
    meta: { requiresAuth: false },
  },
];
