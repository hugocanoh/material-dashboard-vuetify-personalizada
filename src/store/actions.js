export default {
  // se obtiene el token desde el localStorage
  loadTokenLogin ({ commit }) {
    return new Promise((resolve) => {
      let token;
      try {
        token = JSON.parse(localStorage.getItem('tokenUser')) || undefined;
      } catch (err) {
        token = undefined;
      }
      commit('setTokenLogin', token);
      resolve();
    });
  },

};
