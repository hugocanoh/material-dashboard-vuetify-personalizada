const set = (stateName) => (state, payload) => { state[stateName] = payload; };
const toggle = (stateName) => (state) => { state[stateName] = !state[stateName]; };

export default {
  setSidebar: set('sidebar'),
  toggleSidebar: toggle('sidebar'),
  setTokenLogin: set('tokenUser'),
  id_diagnostico: set('tokenUser'),
};
