import Vue from 'vue';

// Lib imports
import Axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, Axios);
