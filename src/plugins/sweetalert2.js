import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';

import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2, {
  confirmButtonColor: '#ff5300',
  cancelButtonColor: '#ff7674',
  position: 'center',
  timer: 3000,
  showConfirmButton: false,
});
