import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import theme from './theme';
import i18n from '@/plugins/i18n';
import '@/sass/overrides.sass';

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    t: (key, ...params) => i18n.t(key, params),
  },
  theme: {
    themes: {
      dark: theme,
      light: theme,
    },
  },
});
