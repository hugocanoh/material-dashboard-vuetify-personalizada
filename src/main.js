// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue';
import App from './App.vue';

import router from '@/router';
import store from '@/store';

// Componentes base disponibles en todo Vue
import './components/base';
// Plugins generales
import './plugins/';
// Plugins base
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n';
import { sync } from 'vuex-router-sync';

// Se registra el Service Worker
import './registerServiceWorker';

// Sync store with router
sync(store, router);

Vue.config.productionTip = false;

// Cargamos información de logueo del usuario
store.dispatch('loadTokenLogin').then(() => {});

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app');
