/* eslint-disable consistent-return */
/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-globals */
importScripts('https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js');
importScripts('sw_utils/sw-db.js');
importScripts('sw_utils/sw-utils.js');

const keyRevision = '7ca37fd5b27f91cd07a2fc68f20787c3';
const APP_SHELL = [
  { revision: keyRevision, url: '/' },
  { revision: keyRevision, url: 'favicon.ico' },
  { revision: keyRevision, url: 'img/icons/icon-72x72.png' },
  { revision: keyRevision, url: 'img/icons/icon-96x96.png' },
  { revision: keyRevision, url: 'img/icons/icon-128x128.png' },
  { revision: keyRevision, url: 'img/icons/icon-144x144.png' },
  { revision: keyRevision, url: 'img/icons/icon-152x152.png' },
  { revision: keyRevision, url: 'img/icons/icon-192x192.png' },
  { revision: keyRevision, url: 'img/icons/icon-384x384.png' },
  { revision: keyRevision, url: 'img/icons/icon-512x512.png' },
];

const STATIC_CACHE = 'static-v1';
const DYNAMIC_CACHE = 'dynamic-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL_INMUTABLE = [
  'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900',
  'https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js ',
];

self.__precacheManifest = APP_SHELL.concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

// omitimos el tiempo de espera para cargar el nuevo SW
// si se ha indicado en el registerServiceWorker.js
self.addEventListener('message', (msg) => {
  if (msg.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});

//  **************************************************
// INSTALACIÓN DEL SERVICE WORKER
self.addEventListener('install', (e) => {
  const cacheStatic = caches.open(STATIC_CACHE)
    .then((cache) => cache.addAll([]));

  const cacheInmutable = caches.open(INMUTABLE_CACHE)
    .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

  e.waitUntil(Promise.all([cacheStatic, cacheInmutable]));
});

//  **************************************************
// ACTIVACIÓN DEL SERVICE WORKER
self.addEventListener('activate', (e) => {
  const respuesta = caches.keys()
    .then((keys) => {
      keys.forEach((key) => {
        if (key !== STATIC_CACHE && key.includes('static')) {
          return caches.delete(key);
        }
        if (key !== DYNAMIC_CACHE && key.includes('dynamic')) {
          return caches.delete(key);
        }
      });
    });
  e.waitUntil(respuesta);
});

//  **************************************************
// MANEJO DE TODAS LAS SOLICITUDES
self.addEventListener('fetch', (e) => {
  let respuesta;
  if (e.request.url.includes('/api')) {
    respuesta = manejoApiMensajes(DYNAMIC_CACHE, e.request);
  } else {
    respuesta = caches.match(e.request)
      .then((res) => {
        if (res) {
          actualizaCacheStatico(STATIC_CACHE, e.request, APP_SHELL_INMUTABLE);
          return res;
        }

        return fetch(e.request)
          .then((newRes) => actualizaCacheDinamico(DYNAMIC_CACHE, e.request, newRes));
      });
  }
  e.respondWith(respuesta);
});

//  **************************************************
// REALIZAR TAREAS ASINCRONAS
self.addEventListener('sync', (e) => {
  console.log('SW: Sync');
  if (e.tag === 'nuevo-post') {
    // postear a BD cuando hay conexión
    const respuesta = postearMensajes();
    e.waitUntil(respuesta);
  }
});

//  **************************************************
// MANEJO DE NOTIFICACIONES PUSH
self.addEventListener('push', (e) => {
  console.log('e', e);
  console.log('e.data.text()', e.data.text());
  const data = JSON.parse(e.data.text());
  console.log('data', data);

  const title = data.titulo;
  const options = {
    body: data.cuerpo,
    icon: 'img/icons/icon-72x72.png',
    badge: 'favicon.ico',
    image: 'https://vignette.wikia.nocookie.net/marvelcinematicuniverse/images/5/5b/Torre_de_los_Avengers.png/revision/latest?cb=20150626220613&path-prefix=es',
    vibrate: [125, 75, 125, 275, 200, 275, 125, 75, 125, 275, 200, 600, 200, 600],
    openUrl: '/',
    data: {
      url: '/',
      id: data.usuario,
    },
    actions: [
      {
        action: 'thor-action',
        title: 'Thor',
      },
      {
        action: 'ironman-action',
        title: 'Ironman',
      },
    ],
  };

  console.log('title', title);
  console.log('options', options);

  e.waitUntil(self.registration.showNotification(title, options));
});

// **************************************************
// CERRAR LAS NOTIFICACIONES
self.addEventListener('notificationclose', (e) => {
  console.log('Notificación cerrada', e);
});

// **************************************************
self.addEventListener('notificationclick', (e) => {
  const notificacion = e.notification;
  const accion = e.action;

  console.log({ notificacion, accion });
  console.log(notificacion);
  console.log(accion);

  const respuesta = clients.matchAll()
    .then((clientes) => {
      const cliente = clientes.find((c) => c.visibilityState === 'visible');

      if (cliente !== undefined) {
        cliente.navigate(notificacion.data.url);
        cliente.focus();
      } else {
        clients.openWindow(notificacion.data.url);
      }

      return notificacion.close();
    });

  e.waitUntil(respuesta);
});
